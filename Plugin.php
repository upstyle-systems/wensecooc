<?php namespace Wense\CooC;

use Backend;
use System\Classes\PluginBase;

/**
 * Plugin Information File
 *
 * @link https://docs.octobercms.com/3.x/extend/system/plugins.html
 */
class Plugin extends PluginBase
{
    /**
     * pluginDetails about this plugin.
     */
    public function pluginDetails()
    {
        return [
            'name' => 'CooC',
            'description' => 'Cookie consent plugin by Wense webdesign',
            'author' => 'Wense',
            'icon' => 'icon-wheelchair-alt'
        ];
    }

    /**
     * register method, called when the plugin is first registered.
     */
    public function register()
    {
    }

    /**
     * boot method, called right before the request route.
     */
    public function boot()
    {
        //
    }

    /**
     * registerComponents used by the frontend.
     */
    public function registerComponents()
    {
        return [
            \Wense\CooC\Components\CookieConsent::class => 'cooc'
        ];
    }

    /**
     * registerPermissions used by the backend.
     */
    public function registerPermissions()
    {
        return [];
    }

    /**
     * registerNavigation used by the backend.
     */
    public function registerNavigation()
    {
        return [];
    }

    public function registerSettings() {
        return [
            'settings' => [
                'label' => 'CooC config',
                'description' => 'Edit CooC config file.',
                'category' => 'CATEGORY_CMS',
                'icon' => 'icon-wheelchair-alt',
                'class' => \Wense\CooC\Models\ConfigFile::class,
            ]
        ];
    }
}
