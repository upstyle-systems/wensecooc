<?php

namespace Wense\CooC\Models;

class ConfigFile extends \System\Models\SettingModel
{
public $settingsCode = 'wense_cooc_configfile';

public $settingsFields = 'fields.yaml';
}
