<?php namespace Wense\CooC\Components;

use Cms\Classes\ComponentBase;
use Wense\CooC\models\ConfigFile;

/**
 * CookieConsent Component
 *
 * @link https://docs.octobercms.com/3.x/extend/cms-components.html
 */
class CookieConsent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Wense Consent',
            'description' => 'Place cookie consent here'
        ];
    }

    public function onRun()
    {
        $this->addCss(['assets/cookieconsent.css']);
        $this->addJs([
            'assets/cookieconsent.umd.js',
//            'assets/cooc-config.js'
        ]);

        $coocConfig = ConfigFile::instance();
        $this->page['cooc_config'] = $coocConfig->cooc_configfile;
    }

    /**
     * @link https://docs.octobercms.com/3.x/element/inspector-types.html
     */
    public function defineProperties()
    {
        return [];
    }
}
